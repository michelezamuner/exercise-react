//import uuid from 'node-uuid';
import AltContainer from 'alt/AltContainer';
import React from 'react';
//import Notes from './Notes';
//import NoteActions from '../actions/NoteActions';
//import NoteStore from '../stores/NoteStore';
import Lanes from './Lanes.jsx';
import LaneActions from '../actions/LaneActions';
import LaneStore from '../stores/LaneStore';

export default class App extends React.Component {
  render() {
    return (
      <div>
        <button onClick={this.addItem}>+</button>
        <AltContainer
          stores={[LaneStore]}
          inject={ {
            items: () => LaneStore.getState().lanes || []
          } }
        >
          <Lanes />
        </AltContainer>
      </div>
    );
  }

  addItem() {
    LaneActions.create({name: 'New lane'});
  }

  /*itemEdited(id, task) {
    if (task) {
      NoteActions.update({id, task});
    }
    else {
      NoteActions.delete(id);
    }
  }*/
}
